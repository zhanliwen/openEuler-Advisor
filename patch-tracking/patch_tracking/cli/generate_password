#!/usr/bin/env python3
"""
command line to generate password hash by pbkdf2
"""

import sys
import re
from werkzeug.security import generate_password_hash


def password_strength_check(password):
    """
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    """

    # calculating the length
    length_error = len(password) < 6

    # searching for digits
    digit_error = re.search(r"\d", password) is None

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None

    # searching for symbols
    symbol_error = re.search(r"[~!@#%^*_+=-]", password) is None

    # overall result
    password_ok = not (length_error or digit_error or uppercase_error or lowercase_error or symbol_error)

    return {
        'ok': password_ok,
        'error': {
            'length': length_error,
            'digit': digit_error,
            'uppercase': uppercase_error,
            'lowercase': lowercase_error,
            'symbol': symbol_error,
        }
    }


ret = password_strength_check(sys.argv[1])
if not ret['ok']:
    print("Password strength is not satisfied.")
    for item in ret['error']:
        if ret['error'][item]:
            print("{} not satisfied.".format(item))
    print(
        """
password strength require:
    6 characters or more
    at least 1 digit [0-9]
    at least 1 alphabet [a-z]
    at least 1 alphabet of Upper Case [A-Z]
    at least 1 special character from [~!@#%^*_+=-]
"""
    )
else:
    print(generate_password_hash(sys.argv[1]))
