---
version_control: pypi
src_repo: path
tag_prefix: "^v"
separator: "."
last_query:
  time_stamp: 2020-04-26 03:29:13.852706750 +00:00
  raw_data: '{"info":{"author":"Jason Orendorff","author_email":"jason.orendorff@gmail.com","bugtrack_url":null,"classifiers":["Development
    Status :: 5 - Production/Stable","Intended Audience :: Developers","License ::
    OSI Approved :: MIT License","Operating System :: OS Independent","Programming
    Language :: Python :: 3","Programming Language :: Python :: 3 :: Only","Topic
    :: Software Development :: Libraries :: Python Modules"],"description":".. image::
    https://img.shields.io/pypi/v/path.svg\n   :target: `PyPI link`_\n\n.. image::
    https://img.shields.io/pypi/pyversions/path.svg\n   :target: `PyPI link`_\n\n..
    _PyPI link: https://pypi.org/project/path\n\n.. image:: https://dev.azure.com/jaraco/path/_apis/build/status/jaraco.path?branchName=master\n   :target:
    https://dev.azure.com/jaraco/path/_build/latest?definitionId=1&branchName=master\n\n..
    image:: https://img.shields.io/travis/jaraco/path/master.svg\n   :target: https://travis-ci.org/jaraco/path\n\n..
    image:: https://img.shields.io/badge/code%20style-black-000000.svg\n   :target:
    https://github.com/psf/black\n   :alt: Code style: Black\n\n.. image:: https://img.shields.io/appveyor/ci/jaraco/path/master.svg\n   :target:
    https://ci.appveyor.com/project/jaraco/path/branch/master\n\n.. image:: https://readthedocs.org/projects/path/badge/?version=latest\n   :target:
    https://path.readthedocs.io/en/latest/?badge=latest\n\n.. image:: https://tidelift.com/badges/package/pypi/path\n   :target:
    https://tidelift.com/subscription/pkg/pypi-path?utm_source=pypi-path&utm_medium=readme\n\n\n``path``
    (aka path pie, formerly ``path.py``) implements path\nobjects as first-class entities,
    allowing common operations on\nfiles to be invoked on those path objects directly.
    For example:\n\n.. code-block:: python\n\n    from path import Path\n\n    d =
    Path(\"/home/guido/bin\")\n    for f in d.files(\"*.py\"):\n        f.chmod(0o755)\n\n    #
    Globbing\n    for f in d.files(\"*.py\"):\n        f.chmod(\"u+rwx\")\n\n    #
    Changing the working directory:\n    with Path(\"somewhere\"):\n        # cwd
    in now `somewhere`\n        ...\n\n    # Concatenate paths with /\n    foo_txt
    = Path(\"bar\") / \"foo.txt\"\n\nPath pie is `hosted at Github <https://github.com/jaraco/path>`_.\n\nFind
    `the documentation here <https://path.readthedocs.io>`_.\n\nGuides and Testimonials\n=======================\n\nYasoob
    wrote the Python 101 `Writing a Cleanup Script\n<http://freepythontips.wordpress.com/2014/01/23/python-101-writing-a-cleanup-script/>`_\nbased
    on ``path``.\n\nAdvantages\n==========\n\nPython 3.4 introduced\n`pathlib <https://docs.python.org/3/library/pathlib.html>`_,\nwhich
    shares many characteristics with ``path``. In particular,\nit provides an object
    encapsulation for representing filesystem paths.\nOne may have imagined ``pathlib``
    would supersede ``path``.\n\nBut the implementation and the usage quickly diverge,
    and ``path``\nhas several advantages over ``pathlib``:\n\n- ``path`` implements
    ``Path`` objects as a subclass of\n  ``str``, and as a result these ``Path``\n  objects
    may be passed directly to other APIs that expect simple\n  text representations
    of paths, whereas with ``pathlib``, one\n  must first cast values to strings before
    passing them to\n  APIs unaware of ``pathlib``. This shortcoming was `addressed\n  by
    PEP 519 <https://www.python.org/dev/peps/pep-0519/>`_,\n  in Python 3.6.\n- ``path``
    goes beyond exposing basic functionality of a path\n  and exposes commonly-used
    behaviors on a path, providing\n  methods like ``rmtree`` (from shlib) and ``remove_p``
    (remove\n  a file if it exists).\n- As a PyPI-hosted package, ``path`` is free
    to iterate\n  faster than a stdlib package. Contributions are welcome\n  and encouraged.\n-
    ``path`` provides a uniform abstraction over its Path object,\n  freeing the implementer
    to subclass it readily. One cannot\n  subclass a ``pathlib.Path`` to add functionality,
    but must\n  subclass ``Path``, ``PosixPath``, and ``WindowsPath``, even\n  if
    one only wishes to add a ``__dict__`` to the subclass\n  instances.  ``path``
    instead allows the ``Path.module``\n  object to be overridden by subclasses, defaulting
    to the\n  ``os.path``. Even advanced uses of ``path.Path`` that\n  subclass the
    model do not need to be concerned with\n  OS-specific nuances.\n\nAlternatives\n============\n\nIn
    addition to\n`pathlib <https://docs.python.org/3/library/pathlib.html>`_, the\n`pylib
    project <https://pypi.org/project/py/>`_ implements a\n`LocalPath <https://github.com/pytest-dev/py/blob/72601dc8bbb5e11298bf9775bb23b0a395deb09b/py/_path/local.py#L106>`_\nclass,
    which shares some behaviors and interfaces with ``path``.\n\nDevelopment\n===========\n\nTo
    install a development version, use the Github links to clone or\ndownload a snapshot
    of the latest code. Alternatively, if you have git\ninstalled, you may be able
    to use ``pip`` to install directly from\nthe repository::\n\n    pip install git+https://github.com/jaraco/path.git\n\nTesting\n=======\n\nTests
    are invoked with `tox <https://pypi.org/project/tox>`_. After\nhaving installed
    tox, simply invoke ``tox`` in a checkout of the repo\nto invoke the tests.\n\nTests
    are also run in continuous integration. See the badges above\nfor links to the
    CI runs.\n\nReleasing\n=========\n\nTagged releases are automatically published
    to PyPI by Azure\nPipelines, assuming the tests pass.\n\nOrigins\n=======\n\nThe
    ``path.py`` project was initially released in 2003 by Jason Orendorff\nand has
    been continuously developed and supported by several maintainers\nover the years.\n\nSecurity
    Contact\n================\n\nTo report a security vulnerability, please use the\n`Tidelift
    security contact <https://tidelift.com/security>`_.\nTidelift will coordinate
    the fix and disclosure.\n\n\n","description_content_type":"","docs_url":null,"download_url":"","downloads":{"last_day":-1,"last_month":-1,"last_week":-1},"home_page":"https://github.com/jaraco/path","keywords":"","license":"","maintainer":"Jason
    R. Coombs","maintainer_email":"jaraco@jaraco.com","name":"path","package_url":"https://pypi.org/project/path/","platform":"","project_url":"https://pypi.org/project/path/","project_urls":{"Homepage":"https://github.com/jaraco/path"},"release_url":"https://pypi.org/project/path/13.2.0/","requires_dist":["importlib-metadata
    (>=0.5) ; python_version < \"3.8\"","sphinx ; extra == ''docs''","jaraco.packaging
    (>=3.2) ; extra == ''docs''","rst.linker (>=1.9) ; extra == ''docs''","pytest
    (!=3.7.3,>=3.5) ; extra == ''testing''","pytest-checkdocs (>=1.2.3) ; extra ==
    ''testing''","pytest-flake8 ; extra == ''testing''","pytest-black-multipy ; extra
    == ''testing''","pytest-cov ; extra == ''testing''","appdirs ; extra == ''testing''","packaging
    ; extra == ''testing''","pygments ; extra == ''testing''"],"requires_python":">=3.6","summary":"A
    module wrapper for os.path","version":"13.2.0","yanked":false},"last_serial":6682777,"releases":{"13.0.0":[{"comment_text":"","digests":{"md5":"2b4be9cc49c8921be1f0f897fd5d3ccb","sha256":"917af40877eb18f814e657e2626731128c831536b2c7562ed8078b8a2e5fe2e9"},"downloads":-1,"filename":"path-13.0.0-py3-none-any.whl","has_sig":false,"md5_digest":"2b4be9cc49c8921be1f0f897fd5d3ccb","packagetype":"bdist_wheel","python_version":"py3","requires_python":">=3.5","size":19346,"upload_time":"2019-11-29T19:03:30","upload_time_iso_8601":"2019-11-29T19:03:30.097078Z","url":"https://files.pythonhosted.org/packages/7c/d3/19effb6328caed6cd54faa0a56c80346a8bdb5fa207c6ff18ff803ddc883/path-13.0.0-py3-none-any.whl","yanked":false},{"comment_text":"","digests":{"md5":"3bdba95b59c2d1611360c40dccf7c752","sha256":"e82f0737e3e185126d686489c74aebdfe14577b3b127b4766141d57aacee3fc8"},"downloads":-1,"filename":"path-13.0.0.tar.gz","has_sig":false,"md5_digest":"3bdba95b59c2d1611360c40dccf7c752","packagetype":"sdist","python_version":"source","requires_python":">=3.5","size":44870,"upload_time":"2019-11-29T19:03:31","upload_time_iso_8601":"2019-11-29T19:03:31.519163Z","url":"https://files.pythonhosted.org/packages/f3/be/012944be9812fa87f78a5d0c07d2137204e10497161a6a9723fb126834c0/path-13.0.0.tar.gz","yanked":false}],"13.1.0":[{"comment_text":"","digests":{"md5":"958b1f0ea23ddaa530f72e9e491702bd","sha256":"41f0db0b6e32b3fc33c0bede630f6b58c7790af3a27c899e0c7ff69143d8696d"},"downloads":-1,"filename":"path-13.1.0-py3-none-any.whl","has_sig":false,"md5_digest":"958b1f0ea23ddaa530f72e9e491702bd","packagetype":"bdist_wheel","python_version":"py3","requires_python":">=3.5","size":19278,"upload_time":"2019-12-01T17:41:12","upload_time_iso_8601":"2019-12-01T17:41:12.216250Z","url":"https://files.pythonhosted.org/packages/4d/24/5827e075036b5bb6b538f71bf39574d4a8024c5df51206cb9d6739e24d94/path-13.1.0-py3-none-any.whl","yanked":false},{"comment_text":"","digests":{"md5":"c8cd9450519c84ba7a61db1d22bdcad5","sha256":"97249b37e5e4017429a780920147200a2215e268c1a18fa549fec0b654ce99b7"},"downloads":-1,"filename":"path-13.1.0.tar.gz","has_sig":false,"md5_digest":"c8cd9450519c84ba7a61db1d22bdcad5","packagetype":"sdist","python_version":"source","requires_python":">=3.5","size":44958,"upload_time":"2019-12-01T17:41:13","upload_time_iso_8601":"2019-12-01T17:41:13.393238Z","url":"https://files.pythonhosted.org/packages/ff/15/3cb6e963733af47dc11289c20215f78ab5dc090f54cb568b891add332694/path-13.1.0.tar.gz","yanked":false}],"13.2.0":[{"comment_text":"","digests":{"md5":"9a85ca1272f641a7b0d56d74f258deb9","sha256":"6168469b98e5531dfb378c384190113d72d8a90f0c65d792ef1d9c9e50fc3282"},"downloads":-1,"filename":"path-13.2.0-py3-none-any.whl","has_sig":false,"md5_digest":"9a85ca1272f641a7b0d56d74f258deb9","packagetype":"bdist_wheel","python_version":"py3","requires_python":">=3.6","size":19444,"upload_time":"2020-02-23T02:30:40","upload_time_iso_8601":"2020-02-23T02:30:40.811975Z","url":"https://files.pythonhosted.org/packages/54/a4/b87be8a3dbfecc6d6092b91495a5b8091a65542754b2f3d14aa01f494da7/path-13.2.0-py3-none-any.whl","yanked":false},{"comment_text":"","digests":{"md5":"6e474e6eafeb1f56391884f38d88d236","sha256":"10a149813a921540fd48ad639ec9157d08c2149ac2bcc399c74304450f70fdf2"},"downloads":-1,"filename":"path-13.2.0.tar.gz","has_sig":false,"md5_digest":"6e474e6eafeb1f56391884f38d88d236","packagetype":"sdist","python_version":"source","requires_python":">=3.6","size":46212,"upload_time":"2020-02-23T02:30:42","upload_time_iso_8601":"2020-02-23T02:30:42.057729Z","url":"https://files.pythonhosted.org/packages/80/be/3240de62eb90ccc98bb2877d4a8da910ba243d13c78e173f93da9f6939e1/path-13.2.0.tar.gz","yanked":false}]},"urls":[{"comment_text":"","digests":{"md5":"9a85ca1272f641a7b0d56d74f258deb9","sha256":"6168469b98e5531dfb378c384190113d72d8a90f0c65d792ef1d9c9e50fc3282"},"downloads":-1,"filename":"path-13.2.0-py3-none-any.whl","has_sig":false,"md5_digest":"9a85ca1272f641a7b0d56d74f258deb9","packagetype":"bdist_wheel","python_version":"py3","requires_python":">=3.6","size":19444,"upload_time":"2020-02-23T02:30:40","upload_time_iso_8601":"2020-02-23T02:30:40.811975Z","url":"https://files.pythonhosted.org/packages/54/a4/b87be8a3dbfecc6d6092b91495a5b8091a65542754b2f3d14aa01f494da7/path-13.2.0-py3-none-any.whl","yanked":false},{"comment_text":"","digests":{"md5":"6e474e6eafeb1f56391884f38d88d236","sha256":"10a149813a921540fd48ad639ec9157d08c2149ac2bcc399c74304450f70fdf2"},"downloads":-1,"filename":"path-13.2.0.tar.gz","has_sig":false,"md5_digest":"6e474e6eafeb1f56391884f38d88d236","packagetype":"sdist","python_version":"source","requires_python":">=3.6","size":46212,"upload_time":"2020-02-23T02:30:42","upload_time_iso_8601":"2020-02-23T02:30:42.057729Z","url":"https://files.pythonhosted.org/packages/80/be/3240de62eb90ccc98bb2877d4a8da910ba243d13c78e173f93da9f6939e1/path-13.2.0.tar.gz","yanked":false}]}'
